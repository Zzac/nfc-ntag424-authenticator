import * as aesjs from "aes-js";
const bt = require("./Crypto/byte-tools").default;
const ac = require("./Crypto/aes-cmac").default;
const crypto = require("crypto");

function required(condition, message) {
  console.assert(condition, message);
}

export default class AuthenticateEVFirst {
  constructor(auth_key, iv) {
    this.auth_key = auth_key;
    this.iv = iv;
    this.rndA = "";
    this.rndB = "";
  }

  createFirstCApdu(key_no) {
    var params = key_no + "00";
    var commandApdu = "9071000002" + params + "00";

    return Buffer.from(commandApdu, "hex");
  }

  firstReponse(rApdu) {
    required("rApdu-length", rApdu.length === 18);
    required(
      "status code 91AF",
      rApdu.slice(-2).equals(Buffer.from("91AF", "hex"))
    );
    var rndBEncrypted = rApdu.slice(0, 16);
    this.rndB = decryptCipher(
      this.auth_key,
      this.iv,
      rndBEncrypted.toString("hex")
    );

    this.rndA = "13C5DB8A5930439FC3DEF9A4C675360F"; // CHANGE THIS TO crypto.randomBytes(16).toString("hex") FOR PRODUCTION
    var rndB_LR = bt.leftRotateOneByte(this.rndB);
    var rndA_and_B = this.rndA + rndB_LR;
    var encryptedA_and_B = encryptText(this.auth_key, this.iv, rndA_and_B);
    var cApduSecond = "90AF000020" + encryptedA_and_B + "00";

    return Buffer.from(cApduSecond, "hex");
  }

  secondResponse(rApdu) {
    required("rApdu-length", rApdu.length === 34);
    required("status code 91AF", rApdu.slice(-2) === "9100");

    var encryptedMessage = rApdu.slice(0, 32);

    var decryptedMessage = decryptCipher(
      this.auth_key,
      this.iv,
      encryptedMessage.toString("hex")
    );

    var ti = decryptedMessage.slice(0, 8);
    var rndA_LR = decryptedMessage.slice(8, 40);
    var pdcap2 = decryptedMessage.slice(40, 52);
    var pcdcap2 = decryptedMessage.slice(52, 64);
    var receivedRndA = bt.rightRotateOneByte(rndA_LR);

    required("generated RndA === decrypted RndA", receivedRndA === this.rndA);

    var rndABytes = Buffer.from(this.rndA, "hex");
    var rndBBytes = Buffer.from(this.rndB, "hex");
    var stream = rndABytes.slice(0, 2);
    var xor = require("buffer-xor");
    stream = Buffer.concat([
      stream,
      xor(rndABytes.slice(2, 8), rndBBytes.slice(0, 6)),
    ]);
    stream = Buffer.concat([stream, rndBBytes.slice(-10)]);
    stream = Buffer.concat([stream, rndABytes.slice(8)]);

    var sv1stream = Buffer.from("A55A00010080", "hex");
    sv1stream = Buffer.concat([sv1stream, stream]);

    var sv2stream = Buffer.from("5AA500010080", "hex");
    sv2stream = Buffer.concat([sv2stream, stream]);

    var kSesAuthEnc = ac
      .aesCmac(this.auth_key, sv1stream)
      .toString("hex")
      .toUpperCase();

    var kSesAuthMac = ac
      .aesCmac(this.auth_key, sv2stream)
      .toString("hex")
      .toUpperCase();

    return {
      ti: ti,
      pdcap2: pdcap2,
      pcdcap2: pcdcap2,
      kSesAuthEnc: kSesAuthEnc,
      kSesAuthMac: kSesAuthMac,
    };
  }
}

function encryptText(key, iv, text) {
  var textHexbytes = aesjs.utils.hex.toBytes(text);
  var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  var encryptedBytes = aesCbc.encrypt(textHexbytes);
  var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);

  return encryptedHex.toUpperCase();
}

function decryptCipher(key, iv, encryptedHex) {
  var encryptedHexbytes = aesjs.utils.hex.toBytes(encryptedHex);
  var aesCbc = new aesjs.ModeOfOperation.cbc(key, iv);
  var decryptedBytes = aesCbc.decrypt(encryptedHexbytes);

  var decryptedHex = aesjs.utils.hex.fromBytes(decryptedBytes);

  return decryptedHex.toUpperCase();
}

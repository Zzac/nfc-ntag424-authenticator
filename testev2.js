import AuthenticateEVFirst from "./authenticate";

function required(condition, message) {
  console.assert(condition, message);
}

function test_ev2() {
  let authev = new AuthenticateEVFirst(
    new Uint8Array(16).fill(0),
    new Uint8Array(16).fill(0)
  );

  required(
    authev
      .createFirstCApdu("00")
      .equals(Buffer.from("9071000002000000", "hex")),
    "Initial C-APDU"
  );

  required(
    authev
      .firstReponse(Buffer.from("A04C124213C186F22399D33AC2A3021591AF", "hex"))
      .equals(
        Buffer.from(
          "90AF00002035C3E05A752E0144BAC0DE51C1F22C56B34408A23D8AEA266CAB947EA8E0118D00",
          "hex"
        )
      ),
    "First R-APDU response"
  );

  var { ti, pdcap2, pcdcap2, kSesAuthEnc, kSesAuthMac } = authev.secondResponse(
    Buffer.from(
      "3FA64DB5446D1F34CD6EA311167F5E4985B89690C04A05F17FA7AB2F08120663910091AF",
      "hex"
    )
  );

  required(ti === "9D00C4DF", "ti");
  required(pdcap2.toString("hex") === "000000000000", "pdcap2");
  required(pcdcap2 === "000000000000", "pcdcap2");
  required(kSesAuthEnc === "1309C877509E5A215007FF0ED19CA564", "kSesAuthEnc");
  required(kSesAuthMac === "4C6626F5E72EA694202139295C7A7FC7", "kSesAuthMac");
}

export default test_ev2;

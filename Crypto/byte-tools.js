function leftRotateOneByte(hex) {
  var result;
  if (typeof hex === "string") {
    result = hex.slice(2) + hex.slice(0, 2);
  } else if (hex instanceof Buffer) {
    result = Buffer.concat([hex.slice(1), hex.slice(0, 1)]);
  } else {
    throw new Error("Value must be provided as a Buffer or string.");
  }
  return result;
}

function rightRotateOneByte(hex) {
  var result;
  if (typeof hex === "string") {
    result = hex.slice(0, -2) + hex.slice(-2);
  } else if (hex instanceof Buffer) {
    result = Buffer.concat([hex.slice(0, -1), hex.slice(-1)]);
  } else {
    throw new Error("Value must be provided as a Buffer or string.");
  }
  return result;
}

function leftShiftOneByte(hex) {
  var result;
  if (typeof hex === "string") {
    result = hex.slice(2) + "00";
  } else if (hex instanceof Buffer) {
    console.log("buffer before" + hex.toString("hex"));
    result = Buffer.concat([hex.slice(1), Buffer.from("00", "hex")]);
    console.log("buffer after" + result.toString("hex"));
  } else {
    throw new Error("Value must be provided as a Buffer or string.");
  }
  return result;
}

/*
 * BitShiftLeft and xor copied from other aes-cmac module
 *
 * From: https://github.com/allan-stewart/node-aes-cmac
 */

function bitShiftLeft(buffer) {
  const shifted = Buffer.alloc(buffer.length);
  const last = buffer.length - 1;

  for (let index = 0; index < last; index++) {
    let value = buffer[index] << 1;

    if (buffer[index + 1] & 0x80) {
      value += 0x01;
    }

    shifted[index] = value;
  }

  shifted[last] = buffer[last] << 1;

  return shifted;
}

function xor(bufferA, bufferB) {
  const length = Math.min(bufferA.length, bufferB.length);
  const output = Buffer.alloc(length);

  for (let index = 0; index < length; index++) {
    const value = bufferA[index] ^ bufferB[index];
    output[index] = value;
  }

  return output;
}

export default {
  leftRotateOneByte,
  rightRotateOneByte,
  leftShiftOneByte,
  bitShiftLeft,
  xor,
};

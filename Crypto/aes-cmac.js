const crypto = require("crypto");
const xor = require("buffer-xor");
const bt = require("./byte-tools").default;
const iv = Buffer.from("00000000000000000000000000000000", "hex");

const blockSize = 16;

function generateSubkeys(key) {
  const cipher = crypto.createCipheriv("aes-128-ecb", key, "");

  const cryptedKey = cipher.update(iv);

  let subkey1 = bt.bitShiftLeft(cryptedKey);
  if (msb(cryptedKey) & 0x80) {
    subkey1 = xor(subkey1, 0x87);
  }

  let subkey2 = bt.bitShiftLeft(subkey1);
  if (msb(subkey1) & 0x80) {
    subkey2 = xor(subkey2, 0x87);
  }

  return { subkey1: subkey1, subkey2: subkey2 };
}

function msb(bytes) {
  return bytes >>> 31;
}
function aes(key, message) {
  const cipher = crypto.createCipheriv(
    "aes-" + key.length * 8 + "-cbc",
    key,
    new Uint8Array(16).fill(0)
  );

  var result = cipher.update(message);

  return result;
}

function aesCmac(key, message) {
  if (!key instanceof Uint8Array) {
    throw new Error("The key needs to be of type Uint8Array");
  }

  const { subkey1, subkey2 } = generateSubkeys(Buffer.from(key, "hex"));
  let numBlocks = Math.ceil(message.length / blockSize);
  var lastBlockRemainder = message.length % blockSize;

  if (numBlocks === 0) {
    numBlocks = 1;
  }

  var messageArray = getMessageArray(message, numBlocks, lastBlockRemainder);

  if (lastBlockRemainder === 0) {
    messageArray[numBlocks - 1] = xor(messageArray[numBlocks - 1], subkey1);
  } else {
    messageArray[numBlocks - 1] = xor(messageArray[numBlocks - 1], subkey2);
  }

  var c = Buffer.from("00000000000000000000000000000000", "hex");

  for (let index = 0; index < numBlocks - 1; index++) {
    let c_xor_m = xor(c, messageArray[index]);
    c = aes(key, c_xor_m);
  }

  let c_xor_m = xor(c, messageArray[numBlocks - 1]);
  c = aes(key, c_xor_m);

  return c.slice(0, 16);
}

function getMessageArray(message, numBlocks, lastBlockRemainder) {
  var index = 0;
  var messageArray = [];
  if (lastBlockRemainder !== 0 || message.length === 0) {
    let padding = "80" + "00".repeat(16 - lastBlockRemainder - 1);
    let appendToMessage = Buffer.from(padding, "hex");
    message = Buffer.concat([message, appendToMessage]);
  }
  for (index = 0; index < numBlocks; index++) {
    let messageBlock = message.slice(
      index * blockSize,
      (index + 1) * blockSize
    );
    messageArray.push(messageBlock);
  }

  return messageArray;
}

export default {
  aesCmac,
  generateSubkeys,
};

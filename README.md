
# EV2 cryptography example

* `AuthenticateEV2` - perform authentication with PICC in JavaScript;

### EV2
In `testev2.js` you will find the testcases, with the information found in 'NTAG 424 DNA and NTAG 424 DNA TagTamper features and hints'. The main code can be found in `authenticate.js`. 

* `AuthenticateEV2` - helper for performing `AuthenticateEV2First` handshake with PICC:
  * `createFirstCApdu` - generate the initial C-APDU to start authentication;
  * `firstReponse` - generate a response to first R-APDU from PICC;
  * `secondResponse` - verify second R-APDU from PICC, initialize authenticated session;


  ## References

- https://github.com/icedevml/nfc-ev2-crypto
- https://www.nxp.com/docs/en/application-note/AN12196.pdf
  
